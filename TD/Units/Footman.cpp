// Fill out your copyright notice in the Description page of Project Settings.

#include "Footman.h"
#include "Components/ArrowComponent.h"


// Sets default values
AFootman::AFootman()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GetArrowComponent()->SetVisibility(false);

}

// Called when the game starts or when spawned
void AFootman::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFootman::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFootman::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

